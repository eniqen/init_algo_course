package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_048 {
    public static int getMaxArrayNumber(int[] mass, int maxIndex) {

        return maxIndex > 0 ? Math.max(mass[maxIndex], getMaxArrayNumber(mass, maxIndex - 1)) : mass[0];
    }
}
