package com.getjavajob.traning.algo.init.nemenkom;

public class Task09_042 {
    public static String getReverseWord(final String word) {
        StringBuilder sb = new StringBuilder(word);
        return sb.reverse().toString();
    }
}
