package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task04_033Test {
    public static void main(String[] args) {
        testIsEvenCondition();
        testIsEvenConditionFail();
        testIsNotEvenCondition();
        testIsNotEvenConditionFail();
    }

    public static void testIsEvenCondition(){
        Assert.assertEquals(true, Task04_033.isEvenCondition(524));
    }
    public static void testIsEvenConditionFail(){
        Assert.assertEquals(true, Task04_033.isEvenCondition(523));
    }

    public static void testIsNotEvenCondition(){
        Assert.assertEquals(true, Task04_033.isNotEvenCondition(521));
    }
    public static void testIsNotEvenConditionFail(){
        Assert.assertEquals(true, Task04_033.isNotEvenCondition(524));
    }
}
