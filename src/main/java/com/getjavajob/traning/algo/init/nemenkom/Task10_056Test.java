package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_056Test {
    public static void main(String[] args) {
        testIsSimple();
    }

    public static void testIsSimple() {
        Assert.assertEquals(true, Task10_056.isSimple(7, 2));
    }
}
