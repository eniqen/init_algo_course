package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task09_042Test {
    public static void main(String[] args) {
        testGetReverseWord();
    }

    public static void testGetReverseWord() {
        Assert.assertEquals("drow", Task09_042.getReverseWord("word"));
    }
}
