package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task11_245Test {
    public static void main(String[] args) {
        testGetNewArray();
    }

    public static void testGetNewArray() {
        int[] testArray = {2, -3, 1, -1, 4, 2, -2, 4, 1, 3};
        int [] resultArray = {-3, -1, -2, 2, 1, 4, 2, 4, 1, 3};
        Assert.assertEquals(resultArray, Task11_245.getNewArray(testArray));
    }
}
