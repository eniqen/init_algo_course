package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_055 {
    public static String toOtherSystem(int number, int system) {
        String[] convertToHex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        if (number == 0) {
            return "";
        }
        return toOtherSystem(number / system, system) + convertToHex[number % system];
    }
}
