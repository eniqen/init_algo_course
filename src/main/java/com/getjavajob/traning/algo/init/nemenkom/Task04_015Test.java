package com.getjavajob.traning.algo.init.nemenkom;


import com.getjavajob.training.algo.util.Assert;

public class Task04_015Test {
    public static void main(String[] args) {
        testGetAgeFirst();
        testGetAgeSecond();
        testGetAgeThree();
    }

    public static void testGetAgeFirst() {
        Assert.assertEquals(29, Task04_015.getAge(12, 2014, 6, 1985));
    }

    public static void testGetAgeSecond() {
        Assert.assertEquals(28, Task04_015.getAge(5, 2014, 6, 1985));
    }

    public static void testGetAgeThree() {
        Assert.assertEquals(29, Task04_015.getAge(6, 2014, 6, 1985));
    }
}
