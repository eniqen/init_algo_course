package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task03_029Test {
    public static void main(String[] args) {
        testGetTaskA();
        testGetTaskAFail();
        testGetTaskB();
        testGetTaskBFail();
        testGetTaskC();
        testGetTaskCFail();
        testGetTaskD();
        testGetTaskDFail();
        testGetTaskE();
        testGetTaskEFail();
        testGetTaskF();
        testGetTaskFail();
    }

    public static void testGetTaskA() {
        Assert.assertEquals(true, Task03_029.getTaskA(3, 7));
    }

    public static void testGetTaskAFail() {
        Assert.assertEquals(true, Task03_029.getTaskA(4, 7));
    }

    public static void testGetTaskB() {
        Assert.assertEquals(true, Task03_029.getTaskB(15, 21));
    }

    public static void testGetTaskBFail() {
        Assert.assertEquals(true, Task03_029.getTaskB(1, 13));
    }

    public static void testGetTaskC() {
        Assert.assertEquals(true, Task03_029.getTaskC(0, 23));
    }

    public static void testGetTaskCFail() {
        Assert.assertEquals(true, Task03_029.getTaskC(1, 3));
    }

    public static void testGetTaskD() {
        Assert.assertEquals(true , Task03_029.getTaskD(-1, -7, -12));
    }

    public static void testGetTaskDFail() {
        Assert.assertEquals(true, Task03_029.getTaskD(1, 12, -8));
    }

    public static void testGetTaskE() {
        Assert.assertEquals(true, Task03_029.getTaskE(15, 3, 6));
    }

    public static void testGetTaskEFail() {
        Assert.assertEquals(true, Task03_029.getTaskE(5, 15, 25));
    }

    public static void testGetTaskF() {
        Assert.assertEquals(true, Task03_029.getTaskF(222, 12, 242));
    }

    public static void testGetTaskFail() {
        Assert.assertEquals(true, Task03_029.getTaskF(1, 8, 99));
    }
}
