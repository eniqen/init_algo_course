package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_042 {
    public static double getPower(double number, int pow) {
        return pow > 0 ? number * getPower(number, --pow) : 1;
    }
}
