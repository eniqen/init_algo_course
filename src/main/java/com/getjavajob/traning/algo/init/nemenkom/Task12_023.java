package com.getjavajob.traning.algo.init.nemenkom;

public class Task12_023 {
    public static int[][] getOptionA(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            array[i][i] = 1;
            array[array.length - 1 - i][i] = 1;
        }
        return array;
    }

    public static int[][] getOptionB(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            array[i][i] = 1;
            array[array.length - 1 - i][i] = 1;
            array[(array.length - 1) / 2][i] = 1;
            array[i][(array.length - 1) / 2] = 1;
        }
        return array;
    }


    public static int[][] getOptionC(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            array[i][i] = 1;
            array[array.length - 1 - i][i] = 1;
            array[i][(array.length - 1) / 2] = 1;
            array[0][i] = 1;
            array[array.length - 1][i] = 1;
            array[1][i] = i > 0 & i < array.length - 1 ? 1 : 0;
            array[array.length - 2][i] = i > 0 & i < array.length - 1 ? 1 : 0;
            array[0][i] = 1;
        }
        return array;
    }
}
