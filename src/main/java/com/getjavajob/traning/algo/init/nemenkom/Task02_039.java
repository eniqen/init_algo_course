package com.getjavajob.traning.algo.init.nemenkom;

public class Task02_039 {
    public static double getCalcAngle(final int hours, final int minutes, final int seconds) {
        int newHours;
        final int HOURS_IN_ONE_DAY = 24;
        final int FULL_ANGLE = 360;
        final int CLOCK_HOURS = 12;
        final int MINUTES_IN_ONE_HOUR = 60;
        final int SECONDS_IN_ONE_MINUTE = 60;
        final double HOURS_DEGREE = FULL_ANGLE / CLOCK_HOURS;
        final double MINUTES_DEGREE = HOURS_DEGREE / MINUTES_IN_ONE_HOUR;
        final double SECONDS_DEGREE = MINUTES_DEGREE / SECONDS_IN_ONE_MINUTE;
        if ((hours < 0 | hours >= HOURS_IN_ONE_DAY) || (minutes >= MINUTES_IN_ONE_HOUR | minutes < 0) || (seconds >= SECONDS_IN_ONE_MINUTE | seconds < 0)) {
            throw new IllegalArgumentException("Please enter the correct data");
        } else {
            newHours = hours >= CLOCK_HOURS && hours < HOURS_IN_ONE_DAY ? hours % CLOCK_HOURS : hours;
            return (newHours * HOURS_DEGREE) + (minutes * MINUTES_DEGREE) + (seconds * SECONDS_DEGREE);
        }
    }
}
