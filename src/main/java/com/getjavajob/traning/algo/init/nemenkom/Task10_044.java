package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_044 {
    public static int computeDigitalRoot(int num) {
        if (num < 10 && num > -10) {
            return Math.abs(num);
        }
        int sum = 0;
        while (num != 0) {
             sum += Math.abs(num % 10);
            num /= 10;
        }
        return computeDigitalRoot(sum);
    }
}
