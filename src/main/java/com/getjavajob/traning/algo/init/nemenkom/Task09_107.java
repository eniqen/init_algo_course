package com.getjavajob.traning.algo.init.nemenkom;

public class Task09_107 {
    public static String replaceCharInWord(final String word) {
        char[] chars = word.toCharArray();
        int indexA = word.indexOf('a');
        int indexO = word.lastIndexOf('o');
        try {
            chars[indexA] = 'o';
            chars[indexO] = 'a';
        } catch (Exception e) {
            System.out.println("Word not such char o or char a");
        }
        return String.valueOf(chars);
    }
}
