package com.getjavajob.traning.algo.init.nemenkom;

public class Task02_013 {
    public static int getReverseNumber(int number) {
        int reverseNumber = 0;
        while (number != 0) {
            reverseNumber = reverseNumber * 10 + number % 10;
            number /= 10;
        }
        return reverseNumber;
    }
}