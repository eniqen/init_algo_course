package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_041Test {
    public static void main(String[] args) {
        testGetFactorial();
    }

    public static void testGetFactorial() {
        Assert.assertEquals(120, Task10_041.getFactorial(5));
    }
}
