package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_042Test {
    public static void main(String[] args) {
        testGetPower();
    }

    public static void testGetPower() {
        Assert.assertEquals(8, Task10_042.getPower(2, 3));
    }
}
