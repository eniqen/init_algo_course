package com.getjavajob.traning.algo.init.nemenkom;

public class Task12_063 {
 public static int[][] getAveragePeople(int[][] array) {
        int[][] calcArray = new int[array.length][2];
        for (int row = 0; row < array.length; row++) {
            int avgCountPeople = 0;
            for (int column = 0; column < array[row].length; column++) {
                avgCountPeople += array[row][column];
            }
            calcArray[row][0] = row + 1;
            calcArray[row][1] = avgCountPeople / array[row].length;
        }
        return calcArray;
    }
}
