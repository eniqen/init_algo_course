package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task06_087Test {
    public static void main(String[] args) {
        testGameScore();
    }

    public static void testGameScore() {
        int[][] points = {
                {1, 2, 1, 1, 2, 2, 1, 2, 1, 0},
                {1, 3, 1, 2, 3, 1, 3, 3, 1, 0}};
        Task06_087.Game game = new Task06_087.Game();
        Assert.assertEquals("TeamB 10 : TeamA 8", game.startGame(points));
    }
}
