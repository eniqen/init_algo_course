package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_043Test {

    public static void main(String[] args) {
        testGetSummTaskA();
        testGetCountTaskB();
    }

    public static void testGetSummTaskA() {
        Assert.assertEquals(3, Task10_043.getSummTaskA(12));
    }

    public static void testGetCountTaskB() {
        Assert.assertEquals(4, Task10_043.getCountTaskB(2234));
    }
}
