package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_051 {
    public static void getProcedureA(int number) {
        if (number > 0) {
            System.out.print(number + " ");
            getProcedureA(number - 1);
        }
        return;
    }

    public static void getProcedureB(int number) {
        if (number > 0) {
            getProcedureB(number - 1);
            System.out.print(number + " ");
        }
        return;
    }

    public static void getProcedureC(int number) {
        if (number > 0) {
            System.out.print(number + " ");
            getProcedureB(number - 1);
        }
        System.out.print(number + " ");
        return;
    }

    public static void main(String[] args) {
        getProcedureA(5);
        System.out.println();
        getProcedureB(5);
        System.out.println();
        getProcedureC(5);
    }
}
