package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task11_158Test {
    public static void main(String[] args) {
        testGetArrayNoDublicates();
    }

    public static void testGetArrayNoDublicates() {
        int[] resultArray = {0, 1, 8, 4, 7, 3, 5, 0, 0, 0, 0};
        int[] testArray = {0, 1, 8, 4, 7, 1, 4, 0, 3, 5, 3};
        Assert.assertEquals(resultArray, Task11_158.getArrayWithNoDublicates(testArray));
    }
}
