package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_048Test {
    public static void main(String[] args) {
        testGetMaxArrayNumber();
    }

    public static void testGetMaxArrayNumber() {
        int[] testArray = new int[]{1000, 1500, 2313123, 10000, 1414, 14124};
        Assert.assertEquals(2313123, Task10_048.getMaxArrayNumber(testArray, 5));
    }
}
