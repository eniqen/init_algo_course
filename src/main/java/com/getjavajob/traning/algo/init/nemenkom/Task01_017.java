package com.getjavajob.traning.algo.init.nemenkom;

public class Task01_017 {

    public double taskO(int x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    public double taskP(int a, int b, int c, int x) {
        return 1 / (Math.sqrt(a * Math.pow(x, 2) + b * x + c));
    }

    public double taskR(int x) {
        return (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
    }

    public double taskS(int x) {
        return Math.abs(x) + Math.abs(x + 1);
    }
}
