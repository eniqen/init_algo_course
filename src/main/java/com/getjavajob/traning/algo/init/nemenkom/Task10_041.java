package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_041 {
    public static int getFactorial(int number) {
        return number > 1 ? getFactorial(number - 1) * number : number;
    }
}
