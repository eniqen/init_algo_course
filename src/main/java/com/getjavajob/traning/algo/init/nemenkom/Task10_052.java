package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_052 {
    public static int reverse(int number) {
        if (number < 10) {
            return number;
        }
        return number % 10 * (int) Math.pow(10, (int) Math.log10((double) number)) + reverse(number / 10);
    }
}
