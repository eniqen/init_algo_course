package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task06_008Test {
    public static void main(String[] args) {
        testGetPowNumbers();
        testGetPowNumbersFail();
    }

    public static void testGetPowNumbers() {
        Assert.assertEquals("1 4 9 16 25", Task06_008.getPowNumbers(33));
    }

    public static void testGetPowNumbersFail() {
        Assert.assertEquals("1 4 9 16 25", Task06_008.getPowNumbers(55));
    }
}
