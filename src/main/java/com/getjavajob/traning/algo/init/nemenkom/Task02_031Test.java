package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task02_031Test {
    public static void main(String[] args) {
        testGetXNumber();
        testGetXNumberFail();
    }

    public static void testGetXNumber(){
        Assert.assertEquals(524, Task02_031.getXNumber(542));
    }
    public static void testGetXNumberFail(){
        Assert.assertEquals(542, Task02_031.getXNumber(542));
    }

}
