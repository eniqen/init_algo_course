package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task02_039Test {
    public static void main(String[] args) {
        testGetCalcAngle();
        testGetCalcAngleFail();
    }

    public static void testGetCalcAngle() {
        Assert.assertEquals(30, Task02_039.getCalcAngle(1, 00, 00));
    }
    public static void testGetCalcAngleFail() {
        Assert.assertEquals(30, Task02_039.getCalcAngle(12, 00, 00));
    }
}
