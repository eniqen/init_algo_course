package com.getjavajob.traning.algo.init.nemenkom;

public class Task05_038 {

    public static double getFullDistance(int countBodyMove) {
        double distance = 0;
        for (int i = 1; i <= countBodyMove; i++) {
            distance += (1.0 / i);
        }
        return distance;
    }

    //todo cheack to correct
    public static double getWay(int countBodyMove) {
        double way = 0;
        for (int i = 1; i <= countBodyMove; i++) {
            if (i % 2 != 0) {
                way += (1.0 / i);
            } else {
                way -= (1.0 / i);
            }
        }
        return way;
    }
}
