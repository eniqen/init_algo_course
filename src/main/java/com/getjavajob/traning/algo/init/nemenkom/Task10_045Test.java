package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_045Test {

    public static void main(String[] args) {
        testGetNMember();
        testGetNSumMember();
    }

    public static void testGetNMember() {
        Assert.assertEquals(11, Task10_045.getNMember(3, 2, 5));
    }

    public static void testGetNSumMember() {
        Assert.assertEquals(35, Task10_045.getNSumMember(3, 2, 5));
    }
}
