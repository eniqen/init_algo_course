package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task05_010Test {

    public static void main(String[] args) {
        testGetConvertingDollarsIntoRubles();
        testGetConvertingDollarsIntoRublesFail();
    }

    public static void testGetConvertingDollarsIntoRubles() {
        double[] testArray = {56.93, 113.86, 170.79, 227.72, 284.65, 341.58, 398.51, 455.44, 512.37, 569.3, 626.23, 683.16, 740.09, 797.02, 853.95, 910.88, 967.81, 1024.74, 1081.67, 1138.6};
        Assert.assertEquals(testArray, Task05_010.getConvertingDollarsIntoRubles(56.93));
    }

    public static void testGetConvertingDollarsIntoRublesFail() {
        double[] testArray = {56.93, 113.86, 123.79, 227.72, 284.65, 341.58, 398.51, 455.44, 512.37, 569.3, 626.23, 683.16, 740.09, 797.02, 853.95, 910.88, 967.81, 1024.74, 1081.67, 1138.6};
        Assert.assertEquals(testArray, Task05_010.getConvertingDollarsIntoRubles(56.93));
    }
}
