package com.getjavajob.traning.algo.init.nemenkom;

public class Task06_008 {
    public static String getPowNumbers(int number) {
        int counter = 1;
        int square = 1;
        StringBuilder sb = new StringBuilder();
        do{
            sb.append(square).append(" ");
            counter++;
            square = (int)Math.pow(counter, 2);
        }while ( square < number);
        return sb.toString().trim();
    }
}
