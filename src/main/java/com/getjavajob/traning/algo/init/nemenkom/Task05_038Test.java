package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task05_038Test {
    public static void main(String[] args) {
        testGetFullDistance();
        testGetWay();
    }

    public static void testGetFullDistance(){
        Assert.assertEquals(5.187377517639621, Task05_038.getFullDistance(100));
    }

    public static void testGetWay(){
        Assert.assertEquals(0.8333333333333333, Task05_038.getWay(3));
    }
}
