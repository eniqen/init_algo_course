package com.getjavajob.traning.algo.init.nemenkom;

public class Task12_025 {
    private static final int ARRAY_SIZE_ROWS = 12;
    private static final int ARRAY_SIZE_COLUMNS = 10;

    public static int[][] getOptionA(int[][] array) {
        int count = 1;
        for (int row = 0; row < array.length; row++) {
            for (int column = 0; column < array[row].length; column++) {
                array[row][column] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionB(int[][] array) {
        int count = 1;
        for (int row = 0; row < array[ARRAY_SIZE_COLUMNS].length; row++) {
            for (int column = 0; column < array.length; column++) {
                array[column][row] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionC(int[][] array) {
        int count = 1;
        for (int row = 0; row < array.length; row++) {
            for (int column = array[row].length - 1; column >= 0; column--) {
                array[row][column] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionD(int[][] array) {
        int count = 1;
        for (int row = 0; row < array[ARRAY_SIZE_COLUMNS].length; row++) {
            for (int column = array.length - 1; column >= 0; column--) {
                array[column][row] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionE(int[][] array) {
        int count = 1;
        for (int row = 0; row < array.length; row++) {
            if (row % 2 == 0) {
                for (int column = 0; column < array[row].length; column++) {
                    array[row][column] = count++;
                }
            } else {
                for (int column = array[row].length - 1; column >= 0; column--) {
                    array[row][column] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionF(int[][] array) {
        int count = 1;
        for (int row = 0; row < array[ARRAY_SIZE_COLUMNS].length; row++) {
            if (row % 2 == 0) {
                for (int column = 0; column < array.length; column++) {
                    array[column][row] = count++;
                }
            } else {
                for (int column = array.length - 1; column >= 0; column--) {
                    array[column][row] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionG(int[][] array) {
        int count = 1;
        for (int row = array.length - 1; row >= 0; row--) {
            for (int column = 0; column < array[row].length; column++) {
                array[row][column] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionH(int[][] array) {
        int count = 1;
        for (int row = array[ARRAY_SIZE_COLUMNS].length - 1; row >= 0; row--) {
            for (int column = 0; column < array.length; column++) {
                array[column][row] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionI(int[][] array) {
        int count = 1;
        for (int row = array.length - 1; row >= 0; row--) {
            for (int column = array[row].length - 1; column >= 0; column--) {
                array[row][column] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionJ(int[][] array) {
        int count = 1;
        for (int row = array[ARRAY_SIZE_COLUMNS].length - 1; row >= 0; row--) {
            for (int column = array.length - 1; column >= 0; column--) {
                array[column][row] = count++;
            }
        }
        return array;
    }

    public static int[][] getOptionK(int[][] array) {
        int count = 1;
        for (int row = array.length - 1; row >= 0; row--) {
            if (row % 2 == 0) {
                for (int column = array[row].length - 1; column >= 0; column--) {
                    array[row][column] = count++;
                }
            } else {
                for (int column = 0; column < array[row].length; column++) {
                    array[row][column] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionL(int[][] array) {
        int count = 1;
        for (int row = 0; row < array.length; row++) {
            if(row % 2 == 0){
                for (int column = array[row].length - 1; column >= 0; column--) {
                    array[row][column] = count++;
                }
            }else{
                for (int column = 0; column < array[row].length; column++) {
                    array[row][column] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionM(int[][] array) {
        int count = 1;
        for (int row = array[ARRAY_SIZE_COLUMNS].length - 1; row >= 0; row--) {
            if(row % 2 == 0) {
                for (int column = array.length - 1; column >= 0; column--) {
                    array[column][row] = count++;
                }
            }else {
                for (int column = 0; column < array.length; column++) {
                    array[column][row] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionN(int[][] array) {
        int count = 1;
        for (int row = 0; row < array[ARRAY_SIZE_COLUMNS].length; row++) {
            if(row % 2 == 0){
                for (int column = array.length - 1; column >= 0; column--) {
                    array[column][row] = count++;
                }
            }else {
                for (int column = 0; column < array.length; column++) {
                    array[column][row] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionO(int[][] array) {
        int count = 1;
        for (int row = array.length - 1; row >= 0; row--) {
            if (row % 2 == 0) {
                for (int column = 0; column < array[row].length; column++) {
                    array[row][column] = count++;
                }
            }else {
                for (int column = array[row].length - 1; column >= 0; column--) {
                    array[row][column] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] getOptionP(int[][] array) {
        int count = 1;
        for (int row = array[ARRAY_SIZE_COLUMNS].length - 1; row >= 0; row--) {
            if (row % 2 == 0){
                for (int column = 0; column < array.length; column++) {
                    array[column][row] = count++;
                }
            }else{
                for (int column = array.length - 1; column >= 0; column--) {
                    array[column][row] = count++;
                }
            }
        }
        return array;
    }
}