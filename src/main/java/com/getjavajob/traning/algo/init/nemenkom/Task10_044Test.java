package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_044Test {
    public static void main(String[] args) {
        testComputeDigitalRoot();
    }

    public static void testComputeDigitalRoot(){
        Assert.assertEquals(3, Task10_044.computeDigitalRoot(444));
    }
}
