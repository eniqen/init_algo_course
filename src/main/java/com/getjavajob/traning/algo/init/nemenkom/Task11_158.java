package com.getjavajob.traning.algo.init.nemenkom;

public class Task11_158 {
    public static int[] getArrayWithNoDublicates(int... array) {
        int endIndex = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length - endIndex; j++) {
                if (array[i] == array[j]) {
                    int index = j;
                    endIndex++;
                    int tempEllement = array[j] = 0;
                    while (j != array.length - 1) {
                        array[j] = array[j + 1];
                        array[j + 1] = tempEllement;
                        j++;
                    }
                    j = --index;
                }
            }
        }
        return array;
    }
}
