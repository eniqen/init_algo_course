package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_053Test {
    public static void main(String[] args) {
        testGetReverseArray();
    }

    private static void testGetReverseArray() {
        //Fixme fix this test
        int[] testArray = {1, 2, 3, 4, 5, 0};
        Assert.assertEquals(054321, Task10_053.getReverseArray(5, testArray));
    }
}
