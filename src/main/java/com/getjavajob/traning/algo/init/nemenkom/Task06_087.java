package com.getjavajob.traning.algo.init.nemenkom;

public class Task06_087 {
    public static class Game {
        private int pointTeamA = 0;
        private int pointTeamB = 0;

        public String startGame(int[][] pointsByTeams) {
            for (int column = 0; column < pointsByTeams[0].length; column++) {
                if (pointsByTeams[0][column] != 0 || pointsByTeams[1][column] != 0) {
                    switch (pointsByTeams[0][column]) {
                        case 1:
                            pointTeamA += pointsByTeams[1][column];
                            break;
                        case 2:
                            pointTeamB += pointsByTeams[1][column];
                            break;
                        default:
                            continue;
                    }
                } else {
                    break;
                }
            }
            return pointTeamA == pointTeamB
                    ? String.format("Draw %d : %d", pointTeamA, pointTeamB)
                    : pointTeamA > pointTeamB
                    ? String.format("TeamA %d : TeamB %d", pointTeamA, pointTeamB)
                    : String.format("TeamB %d : TeamA %d", pointTeamB, pointTeamA);
        }
    }
}
