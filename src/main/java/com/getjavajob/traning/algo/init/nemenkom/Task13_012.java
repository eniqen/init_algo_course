package com.getjavajob.traning.algo.init.nemenkom;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Task13_012 {
    public static List<Employee> listEmployees;

    public static void main(String[] args) {
        DataBase db = new DataBase();
//        System.out.println(listEmployees.get(0).getWasteYears());
//        System.out.println(db.searchByName("bat"));
        System.out.println(db.searchByExperience(5));
    }

    public static class Employee {
        private String lastName;
        private String firstName;
        private String middleName;
        private String address;
        private Date startFirstDay;

        public Employee(String lastName, String firstName, String address, Date startFirstDay) {
            this.lastName = lastName;
            this.firstName = firstName;
            this.address = address;
            this.startFirstDay = startFirstDay;
        }

        public Employee(String lastName, String firstName, String middleName, String address, Date startFirstDay) {
            this(lastName, firstName, address, startFirstDay);
            this.middleName = middleName;
        }

        @Override
        public String toString() {
            return "Employee: " + lastName + " "
                    + firstName + (middleName == null ? "" : " " + middleName) +
                    ", Address: " + address +
                    ", Date start: " + startFirstDay;
        }

        public Date getStartFirstDay() {
            return startFirstDay;
        }

        public void setStartFirstDay(Date startFirstDay) {
            this.startFirstDay = startFirstDay;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Employee employee = (Employee) o;

            if (lastName != null ? !lastName.equals(employee.lastName) : employee.lastName != null) return false;
            if (firstName != null ? !firstName.equals(employee.firstName) : employee.firstName != null) return false;
            if (middleName != null ? !middleName.equals(employee.middleName) : employee.middleName != null)
                return false;
            if (address != null ? !address.equals(employee.address) : employee.address != null) return false;
            return !(startFirstDay != null ? !startFirstDay.equals(employee.startFirstDay) : employee.startFirstDay != null);

        }

        @Override
        public int hashCode() {
            int result = lastName != null ? lastName.hashCode() : 0;
            result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
            result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
            result = 31 * result + (address != null ? address.hashCode() : 0);
            result = 31 * result + (startFirstDay != null ? startFirstDay.hashCode() : 0);
            return result;
        }

        public int getWasteYears() {
            Date currentDate = new Date();
            return getStartFirstDay().getMonth() >= currentDate.getMonth() ? currentDate.getYear() - getStartFirstDay().getYear() : (currentDate.getYear() - 1) - getStartFirstDay().getYear();
        }
    }

    public static class DataBase {
        static {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            listEmployees = new ArrayList<>();
            try {
                listEmployees.add(new Employee("Batareykin", "Vasiliy", "Kukushkina 5", format.parse("30.06.2001")));
                listEmployees.add(new Employee("Semenov", "Pavel", "Sovsemnepavlovich", "Petrushkina 7", format.parse("12.11.2003")));
                listEmployees.add(new Employee("Smibatrnov", "BorBatis", "Kvakushkina 8", format.parse("11.12.2011")));
                listEmployees.add(new Employee("Lozunov", "Lyagun", "SerezBathkina 9", format.parse("12.01.2004")));
                listEmployees.add(new Employee("Petuhov", "Tsiplen", "BogbatRin", "Gnezdovo 3", format.parse("15.12.2010")));
                listEmployees.add(new Employee("Buterov", "Kotlet", "Khlebnaya 5", format.parse("21.03.2001")));
                listEmployees.add(new Employee("Syrnikov", "Lepeh", "Skovorodino 1", format.parse("17.04.2013")));
                listEmployees.add(new Employee("Yaishnica", "Myasnaya", "Gril'yazhnaya 13", format.parse("24.13.2010")));
                listEmployees.add(new Employee("Maiskaya", "Bulka", "Povidlovo 45", format.parse("11.11.1995")));
                listEmployees.add(new Employee("Svetskaya", "L'vitsa", "Pafosovo 1", format.parse("15.07.1999")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        public static String searchByName(String searchType) {
            StringBuilder sb = new StringBuilder();
            for (Employee listEmployee : listEmployees) {
                String middleName = listEmployee.getMiddleName() == null ? "" : listEmployee.getMiddleName() + " ";
                if (listEmployee.getLastName().toLowerCase().contains(searchType.toLowerCase())
                        || listEmployee.getFirstName().toLowerCase().contains(searchType.toLowerCase())
                        || middleName.toLowerCase().contains(searchType)) {
                    sb.append(listEmployee.getFirstName()).append(" ")
                            .append(listEmployee.getLastName()).append(" ")
                            .append(middleName)
                            .append(listEmployee.getAddress()).append(" ")
                            .append(listEmployee.getStartFirstDay()).append("\n");
                }
            }
            return sb.toString();
        }

        public static String searchByExperience(int yearsExperience) {
            StringBuilder sb = new StringBuilder("");
            Date currentDate = new Date();
            for (Employee listEmployee : listEmployees) {
                String middleName = listEmployee.getMiddleName() == null ? "" : listEmployee.getMiddleName() + " ";
                if (listEmployee.getStartFirstDay().getYear() + yearsExperience <= currentDate.getYear()) {
                    sb.append(listEmployee.getFirstName()).append(" ")
                            .append(listEmployee.getLastName()).append(" ")
                            .append(middleName)
                            .append(listEmployee.getAddress()).append(" ")
                            .append(listEmployee.getStartFirstDay()).append("\n");
                }
            }
            return sb.toString();
        }
    }
}
