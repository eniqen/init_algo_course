package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task05_064Test {
    public static void main(String[] args) {
        testGetAvgPopulationDensity();
    }

    public static void testGetAvgPopulationDensity() {
        int[] persons = {100, 150, 124, 244, 654, 353, 456, 223, 776, 245, 555, 242};
        int[] squares = {100, 500, 134, 244, 674, 343, 346, 232, 767, 234, 512, 234};
        Assert.assertEquals(0.9541666666666667, Task05_064.getAvgPopulationDensity(persons, squares));
    }
}
