package com.getjavajob.traning.algo.init.nemenkom;

public class Task04_115 {
    public static String getNameYear(final int year) {
        StringBuilder sb = new StringBuilder();
        final String[] animalTypes = {"Rat", "Cow", "Tiger", "Hare", "Dragon", "Snake", "Horse", "Sheep", "Monkey", "Rooster", "Dog", "Pig"};
        final String[] colorsType = {"Green", "Red", "Yellow", "White", "Black"};
        return sb.append(animalTypes[(year > 4 ? year - 4 : year) % 12]).append(", ").append(colorsType[((year > 4 ? year - 4 : year) % 5) / 2]).toString();
    }
}
