package com.getjavajob.traning.algo.init.nemenkom;

public class Task12_234 {
 public static int[][] deletedRowInArray(int[][] array, int k) {
        for (int row = 0; row < array.length; row++) {
            for (int column = 0; column < array[row].length; column++) {
                if (row == k - 1) {
                    int index = row;
                    array[row][column] = 0;
                    while (row + 1 < array.length) {
                        int tmp = array[row][column];
                        array[row][column] = array[row + 1][column];
                        array[row + 1][column] = tmp;
                        row++;
                    }
                    row = index;
                }
            }
        }
        return array;
    }

    public static int[][] deletedColumnInArray(int[][] array, int s) {
        for (int row = 0; row < array.length; row++) {
            for (int column = 0; column < array[row].length; column++) {
                if (column == s - 1) {
                    int index = column;
                    array[row][column] = 0;
                    while (column + 1 < array[row].length) {
                        int tmp = array[row][column];
                        array[row][column] = array[row][column + 1];
                        array[row][column + 1] = tmp;
                        column++;
                    }
                    column = index;
                }
            }
        }
        return array;
    }
}
