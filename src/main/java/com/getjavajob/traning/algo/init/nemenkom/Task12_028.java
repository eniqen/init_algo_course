package com.getjavajob.traning.algo.init.nemenkom;

public class Task12_028 {
    public static int[][] getSpiralMatrix(int[][] array) {

        int k = 1, c1 = 0, c2 = array.length - 1, r1 = 0, r2 = array.length - 1;
        while (k <= array.length * array.length) {
            for (int i = c1; i <= c2; i++) {
                array[r1][i] = k++;
            }

            for (int j = r1 + 1; j <= r2; j++) {
                array[j][c2] = k++;
            }

            for (int i = c2 - 1; i >= c1; i--) {
                array[r2][i] = k++;
            }

            for (int j = r2 - 1; j >= r1 + 1; j--) {
                array[j][c1] = k++;
            }
            c1++;
            c2--;
            r1++;
            r2--;
        }
        return array;
    }
}
