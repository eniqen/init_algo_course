package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task09_107Test {

    public static void main(String[] args) {
        testReplaceCharInWord();
    }

    public static void testReplaceCharInWord() {
        Assert.assertEquals("popka", Task09_107.replaceCharInWord("potato"));
    }
}
