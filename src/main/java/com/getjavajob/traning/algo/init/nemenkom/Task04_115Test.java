package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task04_115Test {
    public static void main(String[] args) {
        testGetNameYear();
        testGetNameYearFail();
    }

    public static void testGetNameYear() {
        Assert.assertEquals("Rat, Green", Task04_115.getNameYear(1984));
    }

    public static void testGetNameYearFail() {
        Assert.assertEquals("Rat, Green", Task04_115.getNameYear(2));
    }
}
