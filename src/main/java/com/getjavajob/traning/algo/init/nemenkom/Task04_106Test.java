package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task04_106Test {
    public static void main(String[] args) {
        testGetTheSeason();
        testGetTheSeasonFail();
    }

    public static void testGetTheSeason() {
        Assert.assertEquals("Spring", Task04_106.getTheSeason(5));
    }

    public static void testGetTheSeasonFail() {
        Assert.assertEquals("Spring", Task04_106.getTheSeason(8));
    }
}
