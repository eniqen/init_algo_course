package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_050Test {

    public static void main(String[] args) {
        testGetAckermanFunction();
    }

    public static void testGetAckermanFunction() {
        Assert.assertEquals(61, Task10_050.getAckermanFunction(3, 3));
    }
}
