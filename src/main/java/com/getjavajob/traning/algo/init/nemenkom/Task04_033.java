package com.getjavajob.traning.algo.init.nemenkom;

public class Task04_033 {

    public static boolean isEvenCondition(final int number) {
        return number % 2 == 0;
    }

    public static boolean isNotEvenCondition(final int number) {
        return number % 2 != 0;
    }
}
