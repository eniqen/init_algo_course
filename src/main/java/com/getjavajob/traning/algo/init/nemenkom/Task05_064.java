package com.getjavajob.traning.algo.init.nemenkom;

public class Task05_064 {
    //todo fix ?
    public static double getAvgPopulationDensity(int[] persons, int[] squares) {
        final int AREA_COUNT = 12;
        int sumCountPerson = 0;
        int sumSquare = 0;
        for (int i = 0; i < AREA_COUNT; i++) {
            int countPerson = persons[i];
            int square = squares[i];
            sumCountPerson += countPerson;
            sumSquare += square;
        }
        return (double) sumCountPerson / sumSquare;
    }
}
