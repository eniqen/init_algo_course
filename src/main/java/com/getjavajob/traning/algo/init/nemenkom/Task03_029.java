package com.getjavajob.traning.algo.init.nemenkom;

public class Task03_029 {
    /**
     * Record a condition that is true when :
     * a) each of the numbers X and Y is odd ;
     * b) only one of the numbers X and Y is less than 20;
     * c) at least one of the numbers X and Y equals zero ;
     * g) each of the numbers X, Y, Z, negative ;
     * d) only one of the numbers X, Y and Z is a multiple of five ;
     * e) at least one of the numbers X, Y, Z, greater than 100 .

    /**
     * @param x int value describes an argument of method
     * @param y int value describes an argument of method
     * @return true when each of the numbers X and Y unsigned else return false
     */
    public static boolean getTaskA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * @param x int value describes an argument of method
     * @param y int value describes an argument of method
     * @return true when only one of the numbers X and Y is less than 20 else return false
     */
    public static boolean getTaskB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * @param x int value describes an argument of method
     * @param y int value describes an argument of method
     * @return true when at least one of the numbers X and Y equals zero else return false
     */
    public static boolean getTaskC(int x, int y) {
        return (x == 0 || y == 0);
    }

    /**
     * @param x int value describes an argument of method
     * @param y int value describes an argument of method
     * @param z int value describes an argument of method
     * @return true when each of the numbers X, Y, Z is unsigned else return false
     */
    public static boolean getTaskD(int x, int y, int z) {
        return (x < 0 && y < 0 && z < 0);
    }

    /**
     * @param x int value describes an argument of method
     * @param y int value describes an argument of method
     * @param z int value describes an argument of method
     * @return true when only one of the numbers X, Y and Z is a multiple of five else return false
     */
    //Fixme corrected to the right decision
    public static boolean getTaskE(int x, int y, int z) {
        final boolean xValue = x % 5 == 0;
        final boolean yValue = y % 5 == 0;
        final boolean zValue = z % 5 == 0;
        return (xValue ^ yValue ^ zValue) ^ (xValue && yValue && zValue);
    }

    /**
     * @param x int value describes an argument of method
     * @param y int value describes an argument of method
     * @param z int value describes an argument of method
     * @return true when at least one of the numbers X, Y, Z, greater than 100 else return false
     */
    public static boolean getTaskF(int x, int y, int z) {
        return (x > 100 || y > 100 || z > 100);
    }
}
