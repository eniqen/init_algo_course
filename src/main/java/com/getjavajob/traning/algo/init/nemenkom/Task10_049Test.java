package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_049Test {
    public static void main(String[] args) {
        testRecursiveGetIndexOfLargest();
    }

    public static void testRecursiveGetIndexOfLargest(){
        int[] testArray = {1, 5, 2, 3, 0};
        Assert.assertEquals(1, Task10_049.recursiveGetIndexOfLargest(testArray, 0));
    }
}
