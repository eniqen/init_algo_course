package com.getjavajob.traning.algo.init.nemenkom;

import java.util.Stack;

public class Task09_185 {
    private static final char L_PAREN = '(';
    private static final char R_PAREN = ')';

    public static boolean isBalanced(final String line) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == L_PAREN) {
                stack.push(L_PAREN);
            } else if (line.charAt(i) == R_PAREN) {
                if (stack.isEmpty()) {
                    return false;
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        return stack.isEmpty();
    }

    public static String getNotBalancedIndex(final String line) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == L_PAREN) {
                stack.push(L_PAREN);
            } else if (line.charAt(i) == R_PAREN) {
                if (stack.isEmpty()) {
                    return i + " Index";
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        return stack.size() + " count";
    }
}
