package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task12_023Test {
    private static final int ARRAY_INDEX = 7;

    public static void main(String[] args) {
        testGetOptionA();
        testGetOptionB();
        testGetOptionC();
    }

    public static void testGetOptionA() {
        int[][] resultArrayA = new int[ARRAY_INDEX][ARRAY_INDEX];
        int[][] arrayTestA = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        Assert.assertEquals(arrayTestA, Task12_023.getOptionA(resultArrayA));
    }

    public static void testGetOptionB() {
        int[][] resultArrayB = new int[ARRAY_INDEX][ARRAY_INDEX];
        int[][] arrayTestB = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        Assert.assertEquals(arrayTestB, Task12_023.getOptionB(resultArrayB));
    }

    public static void testGetOptionC() {
        int[][] resultArrayC = new int[ARRAY_INDEX][ARRAY_INDEX];
        int[][] arrayTestC = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        Assert.assertEquals(arrayTestC, Task12_023.getOptionC(resultArrayC));
    }
}
