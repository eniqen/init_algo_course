package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_056 {
    public static boolean isSimple(int number, int divider) {
        if (divider > number / 2){
            return true;
        }
        if (number % divider == 0 || number < 2){
            return false;
        }
        return isSimple(number, divider + 1);
    }
}
