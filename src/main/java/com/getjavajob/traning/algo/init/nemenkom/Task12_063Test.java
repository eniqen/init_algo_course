package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task12_063Test {
    public static void main(String[] args) {
        testGetAveragePeople();
    }

    public static void testGetAveragePeople() {
        int[][] resultArray = {
                {1, 33},
                {2, 33},
                {3, 30},
                {4, 34},
                {5, 31},
                {6, 27},
                {7, 39},
                {8, 37},
                {9, 38},
                {10, 31},
                {11, 31}
        };

        int[][] testArray = {
                {42, 22, 31, 40},
                {52, 24, 33, 23},
                {32, 24, 33, 31},
                {33, 27, 37, 40},
                {35, 22, 45, 23},
                {23, 24, 21, 40},
                {54, 28, 37, 40},
                {32, 37, 40, 40},
                {33, 42, 38, 40},
                {32, 23, 32, 40},
                {27, 27, 33, 40}
        };
        Assert.assertEquals(resultArray, Task12_063.getAveragePeople(testArray));
    }
}
