package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_049 {
    static int recursiveGetIndexOfLargest(int[] list, int count) {
        if (count == list.length - 1){
            return count;
        }
        int index = recursiveGetIndexOfLargest(list, count + 1);
        return list[count] > list[index] ? count : index;
    }
}
