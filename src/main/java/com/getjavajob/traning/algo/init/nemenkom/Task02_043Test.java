package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task02_043Test {
    public static void main(String[] args) {
        testGetEqualsNumberByMod();
        testGetEqualsNumberByModFail();

    }

    public static void testGetEqualsNumberByMod() {
        Assert.assertEquals(1, Task02_043.getEqualsNumberByMod(6, 12));
    }

    public static void testGetEqualsNumberByModFail() {
        Assert.assertEquals(1, Task02_043.getEqualsNumberByMod(3, 7));
    }
}
