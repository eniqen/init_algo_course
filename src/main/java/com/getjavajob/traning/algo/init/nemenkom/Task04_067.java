package com.getjavajob.traning.algo.init.nemenkom;

public class Task04_067 {
    public static String getCalculateDay(int day) {
        if (day >= 1 && day <= 365) {
            switch (day % 7) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    return "Workday";
            }
        }
        return "Weekend";
    }
}
