package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_047Test {

    public static void main(String[] args) {
        testGetFibonacciNumber();
    }

    public static void testGetFibonacciNumber(){
        Assert.assertEquals(8, Task10_047.getFibonacciNumber(5));
    }
}
