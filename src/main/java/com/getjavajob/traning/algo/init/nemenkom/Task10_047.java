package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_047 {
    public static int getFibonacciNumber(int number) {
        return number > 2 ? getFibonacciNumber(number - 1) + getFibonacciNumber(number - 2) : number;
    }
}
