package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task04_067Test {
    public static void main(String[] args) {
        testGetCalculateDayFirst();
        testGetCalculateDaySecond();
    }

    public static void testGetCalculateDayFirst() {
        Assert.assertEquals("Workday", Task04_067.getCalculateDay(365));
    }

    public static void testGetCalculateDaySecond() {
        Assert.assertEquals("Weekend", Task04_067.getCalculateDay(7));
    }
}
