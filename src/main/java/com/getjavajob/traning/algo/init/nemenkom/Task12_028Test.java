package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task12_028Test {
    private static final int ARRAY_SIZE = 5;

    public static void main(String[] args) {
        testGetSpiralMatrix();
    }

    public static void testGetSpiralMatrix() {
        int[][] resultArray = new int[ARRAY_SIZE][ARRAY_SIZE];
        int[][] testArray = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        Assert.assertEquals(testArray, Task12_028.getSpiralMatrix(resultArray));
    }
}
