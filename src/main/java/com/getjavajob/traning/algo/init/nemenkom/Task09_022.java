package com.getjavajob.traning.algo.init.nemenkom;

public class Task09_022 {
    public static String getHalfWord(final String word) {
        return word.substring(0, word.length() / 2);
    }
}
