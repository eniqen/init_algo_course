package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task12_234Test {
    public static void main(String[] args) {
        testDeletedRowInArray();
        testDeletedColumnInArray();
    }

    public static void testDeletedRowInArray() {
        int[][] resultArray = {
                {2, 3, 4, 4, 7},
                {4, 3, 2, 7, 3},
                {3, 3, 4, 7, 1},
                {6, 4, 5, 6, 2},
                {0, 0, 0, 0, 0}
        };
        int[][] testArray = {
                {2, 3, 4, 4, 7},
                {4, 3, 2, 7, 3},
                {3, 3, 4, 7, 1},
                {3, 6, 8, 1, 2},
                {6, 4, 5, 6, 2}
        };
        Assert.assertEquals(resultArray, Task12_234.deletedRowInArray(testArray, 4));
    }

    public static void testDeletedColumnInArray() {
        int[][] resultArray = {
                {2, 3, 4, 7, 0},
                {4, 3, 2, 3, 0},
                {3, 3, 4, 1, 0},
                {3, 6, 8, 2, 0},
                {6, 4, 5, 2, 0}
        };
        int[][] testArray = {
                {2, 3, 4, 4, 7},
                {4, 3, 2, 7, 3},
                {3, 3, 4, 7, 1},
                {3, 6, 8, 1, 2},
                {6, 4, 5, 6, 2}
        };
        Assert.assertEquals(resultArray, Task12_234.deletedColumnInArray(testArray, 4));
    }
}
