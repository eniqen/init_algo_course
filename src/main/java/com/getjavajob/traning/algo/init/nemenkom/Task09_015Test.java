package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task09_015Test {
    public static void main(String[] args) {
        testGetCharInWord();
    }

    public static void testGetCharInWord() {
        Assert.assertEquals('v', Task09_015.getCharInWord("privet", 4));
    }
}
