package com.getjavajob.traning.algo.init.nemenkom;

public class Task12_024 {
    public static int[][] getOptionA(int[][] array) {
        for (int row = 0; row < array.length; row++) {
            for (int column = 0; column < array.length; column++) {
                if (row == 0 || column == 0) {
                    array[row][column] = 1;
                } else {
                    array[row][column] = array[row - 1][column] + array[row][column - 1];
                }
            }
        }
        return array;
    }

    public static int[][] getOptionB(int[][] array) {
        for (int row = 0; row < array.length; row++) {
            array[row][0] = row + 1;
            for (int column = 1; column < array.length; column++) {
                array[row][column] = array[row][column - 1] == 6 ? 1 : array[row][column - 1] + 1;
            }
        }
        return array;
    }
}