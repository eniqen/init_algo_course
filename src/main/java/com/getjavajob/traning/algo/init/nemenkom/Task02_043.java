package com.getjavajob.traning.algo.init.nemenkom;

public class Task02_043 {
    public static int getEqualsNumberByMod(final int a, final int b) {
        return (a % b) * (b % a) + 1;
    }
}
