package com.getjavajob.traning.algo.init.nemenkom;

public class Task11_245 {
    public static int[] getNewArray(int... array) {
        int[] newArray = new int[array.length];
        int uEllementIndex = 0;
        int endPosIndex = 1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                newArray[uEllementIndex] = array[i];
                uEllementIndex += 1;
            }
            if (array[array.length - i - 1] >= 0) {
                newArray[array.length - endPosIndex] = array[array.length - i - 1];
                endPosIndex += 1;
            }
        }
        return newArray;
    }
}
