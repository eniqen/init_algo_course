package com.getjavajob.traning.algo.init.nemenkom;

public class Task09_017 {
    public static boolean getStatement(final String word) {
        return word.substring(0, 1).equalsIgnoreCase(word.substring(word.length() - 1, word.length()));
    }
}
