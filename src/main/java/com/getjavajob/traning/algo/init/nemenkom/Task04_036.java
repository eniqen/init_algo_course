package com.getjavajob.traning.algo.init.nemenkom;

public class Task04_036 {

    public static String getColorTrafficLights(int minute) {
        final int color = minute % 5;
        if (color >= 0 && color <= 2){
            return "green";
        }else if(color >= 3 && color <=4){
            return "red";
        }
        return "";
    }
}
