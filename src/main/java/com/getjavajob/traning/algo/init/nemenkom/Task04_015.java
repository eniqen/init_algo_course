package com.getjavajob.traning.algo.init.nemenkom;

public class Task04_015 {
    public static int getAge(int todayMonth, int todayYear, int birthdayMonth, int birthdayYear) {
        return (todayYear - birthdayYear) + (todayMonth < birthdayMonth ? -1 : 0);
    }
}
