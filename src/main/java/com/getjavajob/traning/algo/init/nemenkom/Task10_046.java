package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_046 {
    public static int getNMember(int firstMember, int difference, int nMember) {
        if (nMember == 1) {
            return firstMember;
        }
        int sum = getNMember(firstMember, difference, nMember - 1);
        return sum * difference;
    }

    public static int getNMultMember(int firstMember, int difference, int nMember) {
        if (nMember == 1) {
            return firstMember;
        }
        return getNMultMember(firstMember * difference, difference, nMember - 1) + firstMember;
    }
}
