package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_043 {
    public static int getSummTaskA(int number) {
        return number > 10 ? getSummTaskA(number / 10) + (number % 10) : 1;
    }

    public static int getCountTaskB(int number) {
        return number > 10 ? getCountTaskB(number / 10) + 1 : 1;
    }
}
