package com.getjavajob.traning.algo.init.nemenkom;

import java.util.Scanner;

public class Task05_010 {

    public static double[] getConvertingDollarsIntoRubles(double rate) {
        double[] array = new double[20];
        for (int countDollars = 0; countDollars < 20; countDollars++) {
            array[countDollars] = rate * (countDollars + 1);
        }
        return array;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double rate = scanner.nextDouble();
        getConvertingDollarsIntoRubles(rate);
    }
}