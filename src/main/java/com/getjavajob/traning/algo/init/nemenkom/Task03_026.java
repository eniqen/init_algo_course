package com.getjavajob.traning.algo.init.nemenkom;

public class Task03_026 {
    public boolean getResultOne(final boolean x, final boolean y, final boolean z) {
        return !(x || y) && (!x || !z);
    }

    public boolean getResultTwo(final boolean x, final boolean y, final boolean z) {
        return !(!x || y) || (x && !z);
    }

    public boolean getResultThree(final boolean x, final boolean y, final boolean z) {
        return x || !y && !(x || !z);
    }
}