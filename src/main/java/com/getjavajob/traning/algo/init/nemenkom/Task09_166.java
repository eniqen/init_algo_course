package com.getjavajob.traning.algo.init.nemenkom;

public class Task09_166 {
    public static String reverseStartAndEndWord(final String textLine) {
        StringBuilder sb = new StringBuilder();
        String[] words = textLine.split(" ");
        String temp = words[0];
        words[0] = words[words.length - 1];
        words[words.length - 1] = temp;
        for (String word : words) {
            sb.append(word).append(" ");
        }
        return sb.toString().trim();
    }
}
