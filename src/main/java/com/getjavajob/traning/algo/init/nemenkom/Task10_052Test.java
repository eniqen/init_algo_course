package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_052Test {
    public static void main(String[] args) {
        testReverse();
    }

    private static void testReverse() {
        Assert.assertEquals(987654321, Task10_052.reverse(123456789));
    }
}
