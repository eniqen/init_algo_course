package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task09_185Test {
    public static void main(String[] args) {
        testIsBalanced();
        testGetNotBalancedIndex();
    }

    public static void testIsBalanced() {
        Assert.assertEquals(false, Task09_185.isBalanced("Hello(how)) are(you (man)"));
    }

    public static void testGetNotBalancedIndex() {
        Assert.assertEquals("12 Index", Task09_185.getNotBalancedIndex("Hello(how)are)you (man)"));
    }
}
