package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task02_013Test {
    public static void main(String[] args) {
        testGetReverseNumber();
        testGetReverseNumberFail();
    }

    public static void testGetReverseNumber() {
        Assert.assertEquals(234, Task02_013.getReverseNumber(432));
    }
    public static void testGetReverseNumberFail() {
        Assert.assertEquals(345, Task02_013.getReverseNumber(432));
    }
}

