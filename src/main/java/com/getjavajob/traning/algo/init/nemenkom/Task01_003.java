package com.getjavajob.traning.algo.init.nemenkom;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Task01_003 {
    public static String getInputNumber() {
        StringBuilder sb = new StringBuilder();
        try (Scanner scanner = new Scanner(System.in)) {
            int digit = scanner.nextInt();
            sb.append("You input number: ").append(digit);
        } catch (InputMismatchException e){
            return "";
        }
        return sb.toString();
    }
}
