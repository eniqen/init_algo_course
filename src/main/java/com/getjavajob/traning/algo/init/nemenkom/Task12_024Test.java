package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task12_024Test {
    private static final int ARRAY_SIZE = 6;

    public static void main(String[] args) {
        testGetOptionA();
        testGetOptionB();
    }

    public static void testGetOptionA() {
        int[][] resultArrayA = new int[ARRAY_SIZE][ARRAY_SIZE];
        int[][] testArrayA = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        Assert.assertEquals(testArrayA, Task12_024.getOptionA(resultArrayA));
    }

    public static void testGetOptionB() {
        int[][] resultArrayB = new int[ARRAY_SIZE][ARRAY_SIZE];
        int[][] testArrayB = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        Assert.assertEquals(testArrayB, Task12_024.getOptionB(resultArrayB));
    }
}
