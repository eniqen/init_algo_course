package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task10_046Test {
    public static void main(String[] args) {
        testGetNMember();
        testNMultNMember();
    }

    public static void testGetNMember() {
        Assert.assertEquals(4, Task10_046.getNMember(1, 2, 3));
    }

    public static void testNMultNMember() {
        Assert.assertEquals(7, Task10_046.getNMultMember(1, 2, 3));
    }
}
