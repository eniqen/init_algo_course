package com.getjavajob.traning.algo.init.nemenkom;

public class Task10_050 {
    public static int getAckermanFunction(int m, int n) {
        if (m < 0 || n < 0) {
            throw new IllegalArgumentException();
        } else if (m == 0) {
            return n + 1;
        } else if (n == 0) {
            return getAckermanFunction(m - 1, 1);
        } else {
            return getAckermanFunction(m - 1, getAckermanFunction(m, n - 1));
        }
    }
}
