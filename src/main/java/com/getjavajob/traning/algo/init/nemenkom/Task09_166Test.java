package com.getjavajob.traning.algo.init.nemenkom;

import com.getjavajob.training.algo.util.Assert;

public class Task09_166Test {
    public static void main(String[] args) {
       testReverseStartAndEndWord();
    }

    public static void testReverseStartAndEndWord(){
        Assert.assertEquals("Vasya I'm Hello", Task09_166.reverseStartAndEndWord("Hello I'm Vasya"));
    }
}
